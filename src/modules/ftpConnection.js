const { readdir } = require('fs').promises
const ftp = require('basic-ftp')
const chalk = require('chalk')
const isNameSupported = require('../utils/isNameSupported')
const WhatsAppFile = require('./WhatsAppFile')

const ftpConnection = async (serverData, destFolder) => {
    const client = new ftp.Client()

    try {
        await client.access(serverData)
        console.clear()

        if (!client.closed) {
            console.log('Connected to', chalk.green.bold(serverData.host))
        }

        // Files in the phone's WhatsApp images folder
        await client.cd('WhatsApp Images')
        const files = await client.list()

        //Current Folders (years) in the destFolder
        const destFolderContent = {}
        await (
            await readdir(destFolder, { withFileTypes: true })).forEach(direntObject => {

                //If is a directory and the name is a number for example a year: 2020
                //Is added to the object destFolderContent
                if (direntObject.isDirectory() && !isNaN(direntObject.name)) {
                    destFolderContent[direntObject.name] = []
                }
            }
        )

        for (let index in files) {
            if (isNameSupported(files[index].name)) {
                const fileData = new WhatsAppFile(files[index].name, destFolder)

                const checkingFolders = await fileData.checkYearFolderExistence(destFolderContent)
                // If the folder was created
                if (checkingFolders.succes) {
                    // Add the year folder to the object with an array that will contain the months
                    if (checkingFolders.newItem !== undefined) {
                        checkingFolders.newItem ?
                        destFolderContent[fileData.year] = new Array(fileData.month) :
                        destFolderContent[fileData.year].push(fileData.month)
                    }
                    // Download the file to the folder
                    console.log('Downloading:', index)
                    await client.downloadTo(fileData.dirToImport, fileData.name)
                }
            }
        }

        client.close()
    } catch (error) {
        console.error(error)
        client.close()
    }
}

module.exports = ftpConnection