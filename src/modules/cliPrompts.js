const prompts = require('prompts')
const {stat} = require('fs').promises

const isDir = async (path) => {
    try {
        const stats = await stat(path)
        return stats.isDirectory()
    } catch (e) {
        return false
    }
}

const destFolderPrompt = async () => {
    const response = await prompts({
        type: 'text',
        name: 'destFolder',
        message: 'Where do you want to import the files?',
        validate: async value => await isDir(value) ? true : `${value} is not a directory`
    })

    return response.destFolder.endsWith('/') ? response.destFolder : `${response.destFolder}/`
}

const ftpServerQuestions = [
    {
        type: 'text',
        name: 'host',
        message: 'Host of the FTP server'
    }, {
        type: 'number',
        name: 'port',
        message: 'Port'
    }, {
        type: 'text',
        name: 'user',
        message: 'User'
    }, {
        type: 'password',
        name: 'password',
        message: 'Password'
    }
]

const ftpServerPrompts = async () => await prompts(ftpServerQuestions)

module.exports = {destFolderPrompt, ftpServerPrompts}